<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Plugin administration pages are defined here.
 *
 * @package     mod_videofolder
 * @category    admin
 * @copyright   2023 Nicholas van Rheede van Oudtshoorn <nicholas@pbc.wa.edu.au>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if ($hassiteconfig) {
    $settings = new admin_settingpage('mod_videofolder_settings', new lang_string('pluginname', 'mod_videofolder'));

    // phpcs:ignore Generic.CodeAnalysis.EmptyStatement.DetectedIf
    if ($ADMIN->fulltree) {

        $settings->add(new admin_setting_heading('mod_videofolder_settings/general', 'Google Drive Account', ''));
        $options = [];
        $issuers = \core\oauth2\api::get_all_issuers();
        $options[''] = 'Disabled';
        foreach ($issuers as $issuer) {
            $options[$issuer->get('id')] = s($issuer->get('name'));
        }
        $settings->add(new admin_setting_configselect('mod_videofolder/issuerid',
                                                      'Service Account to Use',
                                                      'Select an account that has access to your media.',
                                                      '',
                                                      $options));
    }

        // TODO: Define actual plugin settings page and add it to the tree - {@link https://docs.moodle.org/dev/Admin_settings}.
}

