<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Library of interface functions and constants.
 *
 * @package     mod_videofolder
 * @copyright   2023 Nicholas van Rheede van Oudtshoorn <nicholas@pbc.wa.edu.au>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Saves a new instance of the mod_videofolder into the database.
 *
 * Given an object containing all the necessary data, (defined by the form
 * in mod_form.php) this function will create a new instance and return the id
 * number of the instance.
 *
 * @param object $moduleinstance An object from the form.
 * @param mod_videofolder_mod_form $mform The form.
 * @return int The id of the newly inserted record.
 */
function videofolder_add_instance($moduleinstance, $mform = null) {
    global $DB;

    $moduleinstance->timecreated = time();

    $id = $DB->insert_record('videofolder', $moduleinstance);

    return $id;
}

/**
 * Updates an instance of the mod_videofolder in the database.
 *
 * Given an object containing all the necessary data (defined in mod_form.php),
 * this function will update an existing instance with new data.
 *
 * @param object $moduleinstance An object from the form in mod_form.php.
 * @param mod_videofolder_mod_form $mform The form.
 * @return bool True if successful, false otherwise.
 */
function videofolder_update_instance($moduleinstance, $mform = null) {
    global $DB;

    $moduleinstance->timemodified = time();
    $moduleinstance->id = $moduleinstance->instance;

    return $DB->update_record('videofolder', $moduleinstance);
}

/**
 * Removes an instance of the mod_videofolder from the database.
 *
 * @param int $id Id of the module instance.
 * @return bool True if successful, false on failure.
 */
function videofolder_delete_instance($id) {
    global $DB;

    $exists = $DB->get_record('videofolder', array('id' => $id));
    if (!$exists) {
        return false;
    }

    $DB->delete_records('videofolder', array('id' => $id));

    return true;
}

/**
 * Returns the lists of all browsable file areas within the given module context.
 *
 * The file area 'intro' for the activity introduction field is added automatically
 * by {@see file_browser::get_file_info_context_module()}.
 *
 * @package     mod_videofolder
 * @category    files
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param stdClass $context
 * @return string[].
 */
function videofolder_get_file_areas($course, $cm, $context) {
    return array();
}

/**
 * File browsing support for mod_videofolder file areas.
 *
 * @package     mod_videofolder
 * @category    files
 *
 * @param file_browser $browser
 * @param array $areas
 * @param stdClass $course
 * @param stdClass $cm
 * @param stdClass $context
 * @param string $filearea
 * @param int $itemid
 * @param string $filepath
 * @param string $filename
 * @return file_info Instance or null if not found.
 */
function videofolder_get_file_info($browser, $areas, $course, $cm, $context, $filearea, $itemid, $filepath, $filename) {
    return null;
}

/**
 * Serves the files from the mod_videofolder file areas.
 *
 * @package     mod_videofolder
 * @category    files
 *
 * @param stdClass $course The course object.
 * @param stdClass $cm The course module object.
 * @param stdClass $context The mod_videofolder's context.
 * @param string $filearea The name of the file area.
 * @param array $args Extra arguments (itemid, path).
 * @param bool $forcedownload Whether or not force download.
 * @param array $options Additional options affecting the file serving.
 */
function videofolder_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, $options = array()) {
    global $DB, $CFG;

    if ($context->contextlevel != CONTEXT_MODULE) {
        send_file_not_found();
    }

    require_login($course, true, $cm);
    send_file_not_found();
}

function videofolder_cm_info_view(cm_info $cm) {
    $cm->set_custom_cmlist_item(true);
    global $DB;
    $dbparams = ['id' => $cm->instance];
    $fields = 'id, name, intro, introformat';
    if (!$videofolder = $DB->get_record('videofolder', $dbparams, $fields)) {
        return false;
    }

    $output = format_module_intro('videofolder', $videofolder, $cm->id, false);

    $courseid = $cm->course;
    $handler = \core_customfield\handler::get_handler('core_course', 'course');
    $datas = $handler->get_instance_data($courseid);
    $folderurl = '';
    $folderid = '';
    foreach ($datas as $data) {
        if (empty($data->get_value())) {
            continue;
        }
        if ($data->get_field()->get('shortname') === 'videofolderurl') {
            $folderurl = $data->get_value();
            if (preg_match('/https.*\/drive\.google\.com.*\/folders\/([^?\/]*)/', $folderurl, $folderidmatches) === 1) {
                if (count($folderidmatches) > 1) {
                    $folderid = $folderidmatches[1];
                }
            }
            break;
        }
    }
    if (!empty($folderid)) {
        $cache = cache::make('mod_videofolder', 'googledrivefolders');
        $medialist = $cache->get($folderid);
        $resourcefilter = trim(strtolower($cm->name));
        $filterlen = strlen($resourcefilter);
        $videohtml = "";
        if (trim(strtolower($cm->name)) == 'showall') {
            $output .= "<h4>All Videos</h4>";
        } else {
            $output .= "<h4>$cm->name Videos</h4>";
        }
        foreach ($medialist as $entryname => $fileentry) {
            if (($resourcefilter == 'showall') || (strncmp($entryname, $resourcefilter, $filterlen) === 0)) {
                if (($resourcefilter !== 'showall') && (strlen($entryname) > $filterlen)) { // Don't show eg Session 12 videos under Session 1!
                    if (is_numeric($entryname[$filterlen])) {
                      continue;
                    }
                }
                $name = pathinfo($fileentry->name, PATHINFO_FILENAME);
                $videohtml .= "<h6>$name</h6>";
                $videohtml .= '<div class="mediaplugin mediaplugin_googledrive">';
                $videohtml .= '  <div class="media_wrapper">';
                $videohtml .= "    <iframe title='$name' loading='lazy' src='https://drive.google.com/file/d/" . $fileentry->id .
                            "/preview'
                            width='100%' height='auto' frameborder='0'
                            webkitallowfullscreen mozallowfullscreen allowfullscreen
                            allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture'></iframe>";
                $videohtml .= '  </div>';
                $videohtml .= '</div>';
            }
        }
        if (empty($videohtml)) {
            $output .= "<div class='alert error'>No videos are available yet</div>";
        } else {
            $output .= $videohtml;
        }
    }
    $cm->set_content($output, true);

}

/**
 * @uses FEATURE_IDNUMBER
 * @uses FEATURE_GROUPS
 * @uses FEATURE_GROUPINGS
 * @uses FEATURE_MOD_INTRO
 * @uses FEATURE_COMPLETION_TRACKS_VIEWS
 * @uses FEATURE_GRADE_HAS_GRADE
 * @uses FEATURE_GRADE_OUTCOMES
 * @param string $feature FEATURE_xx constant for requested feature
 * @return mixed True if module supports feature, false if not, null if doesn't know or string for the module purpose.
 */
function videofolder_supports($feature) {
    switch($feature) {
        case FEATURE_IDNUMBER:
            return true;
        case FEATURE_GROUPS:
            return false;
        case FEATURE_GROUPINGS:
            return false;
        case FEATURE_MOD_INTRO:
            return true;
        case FEATURE_COMPLETION_TRACKS_VIEWS:
            return false;
        case FEATURE_GRADE_HAS_GRADE:
            return false;
        case FEATURE_GRADE_OUTCOMES:
            return false;
        case FEATURE_MOD_ARCHETYPE:
            return MOD_ARCHETYPE_RESOURCE;
        case FEATURE_BACKUP_MOODLE2:
            return true;
        case FEATURE_NO_VIEW_LINK:
            return true;
        case FEATURE_MOD_PURPOSE:
            return MOD_PURPOSE_CONTENT;
        default:
            return null;
    }
}
