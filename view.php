<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Prints an instance of mod_videofolder.
 *
 * @package     mod_videofolder
 * @copyright   2023 Nicholas van Rheede van Oudtshoorn <nicholas@pbc.wa.edu.au>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(__DIR__.'/../../config.php');
require_once(__DIR__.'/lib.php');

// Course module id.
$id = optional_param('id', 0, PARAM_INT);

// Activity instance id.
$v = optional_param('v', 0, PARAM_INT);

if ($id) {
    $cm = get_coursemodule_from_id('videofolder', $id, 0, false, MUST_EXIST);
    $course = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $moduleinstance = $DB->get_record('videofolder', array('id' => $cm->instance), '*', MUST_EXIST);
} else {
    $moduleinstance = $DB->get_record('videofolder', array('id' => $v), '*', MUST_EXIST);
    $course = $DB->get_record('course', array('id' => $moduleinstance->course), '*', MUST_EXIST);
    $cm = get_coursemodule_from_instance('videofolder', $moduleinstance->id, $course->id, false, MUST_EXIST);
}

require_login($course, true, $cm);

$modulecontext = context_module::instance($cm->id);

// $event = \mod_videofolder\event\course_module_viewed::create(array(
// 'objectid' => $moduleinstance->id,
// 'context' => $modulecontext
// ));
// $event->add_record_snapshot('course', $course);
// $event->add_record_snapshot('videofolder', $moduleinstance);
// $event->trigger();

$PAGE->set_url('/mod/videofolder/view.php', array('id' => $cm->id));
$PAGE->set_title(format_string($moduleinstance->name));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_context($modulecontext);

echo $OUTPUT->header();
    global $DB;
    $dbparams = ['id' => $cm->instance];
    $fields = 'id, name, intro, introformat';
if (!$videofolder = $DB->get_record('videofolder', $dbparams, $fields)) {
    return false;
}
   $courseid = $cm->course;
    $handler = \core_customfield\handler::get_handler('core_course', 'course');
    $datas = $handler->get_instance_data($courseid);
    $folderurl = '';
    $folderid = '';
foreach ($datas as $data) {
    if (empty($data->get_value())) {
        continue;
    }
    if ($data->get_field()->get('shortname') === 'videofolderurl') {
        $folderurl = $data->get_value();
        if (preg_match('/https.*\/drive\.google\.com.*\/folders\/([^?\/]*)/', $folderurl, $folderidmatches) === 1) {
            if (count($folderidmatches) > 1) {
                $folderid = $folderidmatches[1];
            }
        }
        break;
    }
}
$output = "$cm->name Videos";
if (!empty($folderid)) {
    $cache = cache::make('mod_videofolder', 'googledrivefolders');
    $cache->delete($folderid);
    $medialist = $cache->get($folderid);
    $resourcefilter = trim(strtolower($cm->name));
    $filterlen = strlen($resourcefilter);
    $videohtml = '';
    if (trim(strtolower($cm->name)) == 'showall') {
            $output .= "<h4>All Videos</h4>";
    } else {
        $output .= "<h4>$cm->name Videos</h4>";
    }
    foreach ($medialist as $entryname => $fileentry) {
        if (($resourcefilter == 'showall') || (strncmp($entryname, $resourcefilter, $filterlen) === 0)) {
            if (strlen($entryname) > $filterlen) {
                if (is_numeric($entryname[$filterlen])) {
                    continue;
                }
            }
            $name = pathinfo($fileentry->name, PATHINFO_FILENAME);
            $videohtml .= "<h4>$name</h4>";
            $videohtml .= '<div class="mediaplugin mediaplugin_googledrive">';
            $videohtml .= '  <div class="media_wrapper">';
            $videohtml .= "    <iframe title='$name' loading='lazy' src='https://drive.google.com/file/d/" . $fileentry->id . "/preview'
                            width='100%' height='auto' frameborder='0'
                            webkitallowfullscreen mozallowfullscreen allowfullscreen
                            allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture'></iframe>";
            $videohtml .= '  </div>';
            $videohtml .= '</div>';
        }
    }
    if (empty($videohtml)) {
           $output .= "<div class='alert error'>No videos are available yet</div>";
    } else {
        $output .= $videohtml;
    }

}
echo "<div>$output</div>";

echo $OUTPUT->footer();
