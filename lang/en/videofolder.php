<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Plugin strings are defined here.
 *
 * @package     mod_videofolder
 * @category    string
 * @copyright   2023 Nicholas van Rheede van Oudtshoorn <nicholas@pbc.wa.edu.au>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'Video Folder';
$string['privacy:metadata'] = 'Video Folder does not store any personal data';
$string['videofolder:access'] = 'Access content';
$string['videofolder:view'] = 'View video folder';

$string['videofoldersettings'] = "Video Folder Settings";
$string['videofoldername'] = "Video Folder Name";
$string['videofoldername_help'] = '';
$string['videofolderfilter'] = "Show Videos starting with";
$string['videofolderfilter_help'] = '';
$string['showallvideos'] = 'Disable filtering';
$string['showallvideos_help'] = "Normally, only videos that start with the filter are shown. Check this box to show everything!";
$string['modulename'] = 'Video Folder';
$string['modulename_help'] = "Displays videos according to the course video folder.";
$string['modulenameplural'] = 'Video Folders';
$string['name'] = 'Video Folder';

$string['cachedef_googledrivefolders'] = "Cache of Google Drive Folder listings";
$string['pluginadministration'] = '';
$string['videofolder:addinstance'] = 'Add new videofolder';

$string['cronflush'] = "Flush Google Drive Listing Cache";
