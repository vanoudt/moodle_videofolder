<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Cached data source the videofolder plugin
 *
 * @package    mod_videofolder
 * @copyright 2023 Nicholas van Rheede van Oudtshoorn <nicholas@pbc.wa.edu.au>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_videofolder\cache;

use cache_definition;

/**
 * Class quiz_overrides
 *
 * @package   mod_videofolder
 * @copyright 2023 Nicholas van Rheede van Oudtshoorn <nicholas@pbc.wa.edu.au>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class foldersource implements \cache_data_source
{

    /** @var overrides the singleton instance of this class. */
    protected static $instance = null;

    /**
     * Returns an instance of the data source class that the cache can use for loading data using the other methods
     * specified by this interface.
     *
     * @param cache_definition $definition
     * @return object
     */
    public static function get_instance_for_cache(cache_definition $definition): foldersource
    {
        if (is_null(self::$instance)) {
            self::$instance = new foldersource();
        }
        return self::$instance;
    }

    /**
     * Loads the data for the key provided ready formatted for caching.
     *
     * @param string|int $key The key to load.
     * @return mixed What ever data should be returned, or false if it can't be loaded.
     * @throws \coding_exception
     */
    public function load_for_cache($key)
    {
        $medialist = array();

        $issuerid = get_config('mod_videofolder', 'issuerid');
        if (empty($issuerid) || (!is_numeric($issuerid))) {
            return $medialist;
        }

        $issuer = \core\oauth2\api::get_issuer($issuerid);
        $client = \core\oauth2\api::get_system_oauth_client($issuer);
        $service = new \repository_googledocs\rest($client);
        $params = [
            'q' => "'$key' in parents",
        ];

        $folderlist = $service->call('list', $params);
        foreach ($folderlist->files as $fileentry) {
            $majormimetype = trim(explode("/", $fileentry->mimeType)[0]);
            if ($majormimetype == 'video') {
                $medialist[trim(strtolower($fileentry->name))] = $fileentry;
            }
        }
        ksort($medialist, SORT_NATURAL | SORT_FLAG_CASE);

        return $medialist;
        error_log(print_r($medialist, true));
    }

    /**
     * Loads several keys for the cache.
     *
     * @param array $keys An array of keys each of which will be string|int.
     * @return array An array of matching data items.
     */
    public function load_many_for_cache(array $keys)
    {
        $results = [];

        foreach ($keys as $key) {
            $results[] = $this->load_for_cache($key);
        }

        return $results;
    }
}
